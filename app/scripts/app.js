'use strict';

/**
 * @ngdoc overview
 * @name proofOfConceptApp
 * @description
 * # proofOfConceptApp
 *
 * Main module of the application.
 */
angular
  .module('proofOfConceptApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
