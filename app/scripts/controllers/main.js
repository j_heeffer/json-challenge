'use strict';

/**
 * @ngdoc function
 * @name proofOfConceptApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the proofOfConceptApp
 */
angular.module('proofOfConceptApp')
  .controller('MainCtrl', function ($scope,$http) {

      function search(data, term, path) {
          // if not found loop
          if (!$scope.found) {
              // if term is found
              if (data === term) {
                  // set outcome
                  $scope.outcome = path;
                  // set found
                  $scope.found = true;
              }

              // check if object or array
              if (angular.isObject(data) || angular.isArray(data)) {
                  // loop over data
                  _.forEach(data, function (val, key) {
                      // recursive func with new path string
                      if (search(val, term, path + " " + key)) {
                          return true;
                      }
                  });
              }

              // else string
          }
      }

      $scope.calculatePath = function (data,term) {
          // outcome var
          $scope.outcome = "";

          // unused delimited
          $scope.delimiter = " -> ";

          // flag to designate when we're dne
          $scope.found = false;

          // parse date and create search criteria
          var data = data || JSON.parse($scope.input),
              term = term || $scope.term;

          // start search
          search(data, term, "");
      };

      $scope.fetchC1 = function() {
          $http.get('https://gist.githubusercontent.com/anonymous/8f35cc4fbbccf6d3f59f/raw/1f9786fc2fec9a7afa20cdd70d2d8afb7d3aecb9/challenge1.txt')
              .success(function(data) {
              $scope.calculatePath(data,"dailyprogrammer");
          })
      };
      $scope.fetchC2 = function() {
          $http.get('https://gist.githubusercontent.com/anonymous/b7733192c0d1004a084b/raw/b5f8df53469410c634034c12d99bbb8ccc46f102/challenge2.txt')
              .success(function(data) {
              $scope.calculatePath(data,"dailyprogrammer");
          })
      };
  });



     